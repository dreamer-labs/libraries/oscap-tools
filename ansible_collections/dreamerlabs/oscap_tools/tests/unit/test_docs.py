import os
import subprocess
import pytest

pth = os.path.realpath(os.path.join(__file__, '..', '..', '..', 'plugins', 'modules'))


@pytest.mark.parametrize('module_name', ['oscap_vuln_scan', 'oscap_checklist'])
def test_ansible_docs(module_name):

    docs = subprocess.check_output(['ansible-doc', '-M', pth, module_name])

    assert module_name.upper() in str(docs)
