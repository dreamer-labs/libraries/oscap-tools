import json
import pytest
import sys
import os
import shutil

from ansible.module_utils import basic
from ansible.module_utils._text import to_bytes

pth = os.path.realpath(os.path.join(__file__, '..', '..', '..', '..', 'plugins', 'modules'))
sys.path.append(pth)

import oscap_checklist  # noqa E402

FIXTURES_PATH = 'tests/unit/oscap_checklist/fixtures'


def set_module_args(args, check_mode=False):
    if '_ansible_remote_tmp' not in args:
        args['_ansible_remote_tmp'] = '/tmp'
    if '_ansible_keep_remote_files' not in args:
        args['_ansible_keep_remote_files'] = False

    if check_mode:
        args['_ansible_check_mode'] = True

    args = json.dumps({'ANSIBLE_MODULE_ARGS': args})
    basic._ANSIBLE_ARGS = to_bytes(args)


class AnsibleExitJson(Exception):
    """Exception class to be raised by module.exit_json and caught by the test case"""
    pass


class AnsibleFailJson(Exception):
    pass


def exit_json(self, **kwargs):
    if 'changed' not in kwargs:
        kwargs['changed'] = False
    raise AnsibleExitJson(kwargs)


def fail_json(self, *message, **kwargs):
    if message and not isinstance(message[0], str):
        raise Exception("first param for fail_json must be error message")
    kwargs['failed'] = True
    raise AnsibleFailJson(kwargs)


@pytest.fixture
def oscap_bin_fake():
    bin_dir = os.path.realpath(
        os.path.join(__file__, '..', 'fixtures')
    )
    oscap_src = os.path.join(
        bin_dir, 'oscap_bin_good.sh'
    )
    oscap_tgt = os.path.join(bin_dir, 'oscap')

    os.link(oscap_src, oscap_tgt)

    old_path = os.environ['PATH']
    os.environ['PATH'] = os.environ['PATH'] + ':' + bin_dir

    yield oscap_tgt

    os.unlink(oscap_tgt)
    os.environ['PATH'] = old_path


@pytest.mark.parametrize('checkmode', [False, True])
def test_normal_args(monkeypatch, checkmode):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'state': 'pass',
        'rule': 'xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
        },
        check_mode=checkmode
    )

    with pytest.raises(AnsibleExitJson) as exitception:
        oscap_checklist.run_module()

    assert ('xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
            in exitception.value.args[0]['matched_rules'])


# @pytest.mark.parametrize('checkmode', [False, True])
# def test_from_results_not_xml(monkeypatch, checkmode):
#     monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
#     monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
#     set_module_args({
#         'from_results': 'dfasdfasdf',
#         'state': 'pass',
#         'rule': 'xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
#         },
#         check_mode=checkmode
#     )
#
#     with pytest.raises(AnsibleFailJson):
#         oscap_checklist.run_module()


# @pytest.mark.parametrize('checkmode', [False, True])
# def test_from_results_xml(monkeypatch, checkmode):
#     monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
#     monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
#     with open('{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
#               FIXTURES_PATH=FIXTURES_PATH)) as results_xml:
#         set_module_args({
#             'from_results': results_xml.read(),
#             'state': 'pass',
#             'rule': 'xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
#             },
#             check_mode=checkmode
#         )
#
#     with pytest.raises(AnsibleExitJson):
#         oscap_checklist.run_module()


@pytest.mark.parametrize('rules,state', [
    [['xccdf_mil.disa.stig_rule_SV-214799r505924_rule',
      'xccdf_mil.disa.stig_rule_SV-204393r505924_rule'], 'pass'],
    [['xccdf_mil.disa.stig_rule_SV-204407r505924_rule',
      'xccdf_mil.disa.stig_rule_SV-204408r505924_rule'], 'fail']
])
@pytest.mark.parametrize('checkmode', [False, True])
def test_normal_exits_rule_states(monkeypatch, checkmode, rules, state):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH
        ),
        'state': state,
        'rule': rules
        },
        check_mode=checkmode
    )

    with pytest.raises(AnsibleExitJson) as exitception:
        oscap_checklist.run_module()

    assert len(exitception.value.args[0]['unmatched_rules']) == 0


@pytest.mark.parametrize('checkmode', [False, True])
def test_from_results_w_results_file(monkeypatch, tmp_path, checkmode):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)

    results_dir = tmp_path / 'test_results_file'
    results_file = os.path.join(str(results_dir), 'tmp_output.xml')

    set_module_args({
        'from_results': 'results_xml_string',
        'results_file': results_file,
        'state': 'pass',
        'rule': 'xccdf_mil.disa.stig_rule_SV-204408r505924_rule'
        },
        check_mode=checkmode
    )

    with pytest.raises(AnsibleFailJson):
        oscap_checklist.run_module()


@pytest.mark.parametrize('rules,state', [
    [['xccdf_mil.disa.stig_rule_SV-214799r505924_rule',
      'xccdf_mil.disa.stig_rule_SV-204393r505924_rule'], 'fail'],
    [['xccdf_mil.disa.stig_rule_SV-204407r505924_rule',
      'xccdf_mil.disa.stig_rule_SV-204408r505924_rule'], 'pass']
])
def test_failed_exits_rule_states(monkeypatch, rules, state):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'state': state,
        'rule': rules
        }
    )

    with pytest.raises(AnsibleFailJson):
        oscap_checklist.run_module()


@pytest.mark.parametrize('from_results_mode', [
    'path',
    'string'
])
def test_diff_mode(monkeypatch, from_results_mode):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)

    if from_results_mode == 'path':
        compare_results = '{FIXTURES_PATH}/test-xccdf-scan1-diff.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH)
    else:
        with open('{FIXTURES_PATH}/test-xccdf-scan1-diff.xml'.format(
                  FIXTURES_PATH=FIXTURES_PATH)) as comp_file:
            compare_results = comp_file.read()

    set_module_args({
        'compare_results': compare_results,
        'from_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        }
    )

    with pytest.raises(AnsibleExitJson) as exitception:
        oscap_checklist.run_module()

    assert ('xccdf_mil.disa.stig_rule_SV-204633r505924_rule'
            in str(exitception.value.args[0]['diff']))

    assert ('xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
            in str(exitception.value.args[0]['diff']))


def test_diff_mode_no_diff(monkeypatch):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'compare_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'from_results': '{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        }
    )

    with pytest.raises(AnsibleExitJson) as exitception:
        oscap_checklist.run_module()

    assert ('xccdf_mil.disa.stig_rule_SV-204633r505924_rule'
            not in str(exitception.value.args[0]['diff']))

    assert ('xccdf_mil.disa.stig_rule_SV-214799r505924_rule'
            not in str(exitception.value.args[0]['diff']))


def test_exclusive_args_fail(monkeypatch):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'checklist': 'asdfasdf',
        'from_results': 'results_xml_string',
        'compare_results': 'results_string',
        'results_file': 'tmp-results.xml',
        'state': 'pass',
        'rule': 'v12345',
        'oscap_bin': '/usr/bin/oscap'
        }
    )

    with pytest.raises(AnsibleFailJson) as failz:
        oscap_checklist.run_module()

    assert 'mutually exclusive: checklist|from_results' in str(failz.value)


def test_oscap_eval(monkeypatch, oscap_bin_fake):

    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'checklist': '{FIXTURES_PATH}/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'checklist_profile': 'xccdf_mil.disa.stig_profile_MAC-3_Sensitive',
        'state': 'pass',
        'rule': 'xccdf_mil.disa.stig_rule_SV-204633r505924_rule'}
    )

    with pytest.raises(AnsibleFailJson) as failz:
        oscap_checklist.run_module()

    assert failz.value.args[0]['changed'] is False
    assert ('xccdf_mil.disa.stig_rule_SV-204633r505924_rule'
            in failz.value.args[0]['unmatched_rules'])


@pytest.mark.parametrize('checkmode', [False, True])
@pytest.mark.parametrize('oscap_bin_path,exit_code', [
    ('{FIXTURES_PATH}/oscap_bin_good.sh'.format(
     FIXTURES_PATH=FIXTURES_PATH), 0),
    ('{FIXTURES_PATH}/oscap_bin_error_code.sh'.format(
     FIXTURES_PATH=FIXTURES_PATH), 2),
    ('{FIXTURES_PATH}/oscap_no_bin'.format(
     FIXTURES_PATH=FIXTURES_PATH), 127)
])
def test_oscap_bin_test_paths(monkeypatch, checkmode, oscap_bin_path, exit_code):
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)

    set_module_args({
        'checklist': '{FIXTURES_PATH}/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'checklist_profile': 'xccdf_mil.disa.stig_profile_MAC-3_Sensitive',
        'oscap_bin': oscap_bin_path,
        'state': 'fail',
        'rule': 'xccdf_mil.disa.stig_rule_SV-204408r505924_rule'
        },
        check_mode=checkmode
    )

    if exit_code == 0:
        AnsibleExitClass = AnsibleExitJson
    else:
        AnsibleExitClass = AnsibleFailJson

    with pytest.raises(AnsibleExitClass):
        oscap_checklist.run_module()


@pytest.mark.parametrize('checkmode', [False, True])
def test_oscap_out_file(monkeypatch, tmp_path, checkmode):
    arg_checks = []

    out_results_dir = tmp_path / "out_results"
    out_results_dir.mkdir()

    out_results_path = '{out_results_dir}/results-test.xml'.format(
        out_results_dir=out_results_dir)

    def run_command_checks(self, oscap_args, check_rc=False):
        arg_checks.extend(oscap_args)
        if out_results_path in oscap_args:
            shutil.copy('{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
                FIXTURES_PATH=FIXTURES_PATH), out_results_path)
            return 0, 'not valid content, look in {out_results_path}'.format(
                out_results_path=out_results_path), ''
        else:
            with open('{FIXTURES_PATH}/test-xccdf-scan1.xml'.format(
                      FIXTURES_PATH=FIXTURES_PATH)) as scanfile:
                return 0, scanfile.read(), ''

    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'fail_json', fail_json)
    monkeypatch.setattr(oscap_checklist.AnsibleModule, 'run_command', run_command_checks)

    set_module_args({
        'checklist': '{FIXTURES_PATH}/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'checklist_profile': 'xccdf_mil.disa.stig_profile_MAC-3_Sensitive',
        'oscap_bin': '{FIXTURES_PATH}/oscap_bin_good.sh'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'results_file': out_results_path,
        'state': 'fail',
        'rule': 'xccdf_mil.disa.stig_rule_SV-204408r505924_rule'
        },
        check_mode=checkmode
    )

    with pytest.raises(AnsibleExitJson):
        oscap_checklist.run_module()

    results_actual = '-' if checkmode else out_results_path

    assert results_actual in arg_checks

    if checkmode:
        assert not os.path.exists(out_results_path)
