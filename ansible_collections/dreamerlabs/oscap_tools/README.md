# Ansible Collection - dreamerlabs.oscap_tools


## dreamerlabs.oscap_tools.oscap_checklist

Performs an OpenSCAP XCCDF scan on a system and compares the results against a list of pass/fail rules


## dreamerlabs.oscap_tools.oscap_vuln_scan

Performs and OpenSCAP OVAL vulnerability scan on a system and returns a list of failed results. This list can be filtered by severity.
